import java.io.IOException;
import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) { 

		TxtToArff converter = new TxtToArff();

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter number of files");
		int number = scan.nextInt();

		for (int i = 0; i < number; i++) {
			
			System.out.println("Enter txt file name - " + (i + 1) + ": ");
			
			try {
				
				String fileN = scan.next();
				converter.setImef(fileN);
				converter.convert();
			
			} catch (IOException e) {
				i--;
			}
		}
		System.out.println("--------------end-----------------");
		scan.close();
	}
}
