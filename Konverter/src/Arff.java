import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Arff {

	private String relation;
	private List<String> attribute;
	private List<String> data;
	private String name;

	public Arff(String relation, List<String> attribute, List<String> data, String name) {
		super();
		this.relation = relation;
		this.attribute = attribute;
		this.data = data;
		this.name = name;
	}

	public String getRealtion() {
		return relation;
	}

	public void setRealtion(String relation) {
		this.relation = relation;
	}

	public List<String> getAttribute() {
		return attribute;
	}

	public void setAttribute(List<String> attribute) {
		this.attribute = attribute;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	public void generateArffFile() throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("@RELATION " + relation + "\n\n");
		for (String s : attribute) {
			sb.append("@ATTRIBUTE " + s + "\n");
		}
		sb.append("\n");
		sb.append("@DATA\n");
		for (String s : data) {
			sb.append(s + "\n");
		}

		// open new file and write sb in it, close file after that
		BufferedWriter out = null;

		File convertedFilesDir = new File("converted files");

		if (!convertedFilesDir.exists()) {
			try {
				convertedFilesDir.mkdir();
			} catch (SecurityException se) {
				System.err.println("Directory creating error: " + se.getMessage());
			}
		}

		File f = new File(convertedFilesDir, name + ".arff");

		try {
			f.createNewFile();

			FileWriter fstream = new FileWriter(f, true); // true tells to
															// append data.
			out = new BufferedWriter(fstream);
			out.write(sb.toString());
		} catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}
}
