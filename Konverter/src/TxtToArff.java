import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TxtToArff {

	private String imef;

	public TxtToArff() {
	}

	public TxtToArff(String imef) {
		this.setImef(imef);
	}

	public void convert() throws IOException {
		List<String> dataRows = new ArrayList<>();

		BufferedReader br = new BufferedReader(new FileReader(imef));

		String line = br.readLine();
		Set<Integer> listOfNominal = new HashSet<>();

		while (line != null) {

			line = line.replaceAll("<>", " .");
			line = line.replaceAll(" ;", ";");

			int index = line.indexOf("_");
			String id = line.substring(0, index);
			listOfNominal.add(Integer.parseInt(id));

			String content = line.substring(line.indexOf(" "), line.length()).trim();

			dataRows.add(id + ", \"" + content + "\"");
			line = br.readLine();
		}

		StringBuilder id = new StringBuilder();
		id.append("\t{");
		for (Integer i : listOfNominal) {
			id.append(i + ", ");
		}
		int indexForDelete = id.lastIndexOf(", ");
		id.deleteCharAt(indexForDelete);
		id.append("}");

		List<String> attributes = new ArrayList<>();
		attributes.add("@@id@@" + id.toString());
		attributes.add("content \t STRING");
		
		String newName = imef.substring(0, imef.indexOf("."));

		Arff a = new Arff("DataMining", attributes, dataRows, newName);
		a.generateArffFile();
		br.close();
	}

	public String getImef() {
		return imef;
	}

	public void setImef(String imef) {
		this.imef = imef;
	}
}
